<?php
/**
 * xMeta Component for Oracle Service Cloud CP3
 * Provides the ability to define custom Page Meta tags.
 * Custom tags handlers are defined through a plugin interface.
 * Based on http://cxdeveloper.com/article/creating-custom-page-meta-tag-attributes-cp3
 * 
 * @copyright (c) 2014, 45 North Solutions LLC
 * @package https://bitbucket.org/45north/xmeta
 * @version 2.0.0
 * @license MIT
 * @author Andy Rowse <andy45n.co>
 */
namespace Custom\Models\xMeta\hooks;


use RightNow\Utils\Url as Url;
use RightNow\Utils\Framework as Framework;

require_once APPPATH . 'models/custom/xMeta/classes/PluginInterface.php';

/**
 * Hook Handler for xMeta component. Activate by adding this code to the hooks.php file
 * 
$rnHooks['pre_page_render'][] = array(
    'class' => 'PrePageRender',
    'function' => 'PageMetaTags',
    'filepath' => 'xMeta/hooks'
);
 * 
 */
class PrePageRender extends \RightNow\Models\Base
{
    
    /**
     * Parse custom page meta tags. Load plugins and execute.
     * Plugins are stored in the models/custom/xMeta/plugins folder
     * and must implement the Custom\Models\xMeta\classes\PluginInterface interface.
     * All plugins must start with a x_ prefix and the name of both the file
     * and the class must match the attribute name. For example, the x_redirect
     * handler would have an attribute of 'x_redirect', a file at
     * 'models/custom/xMeta/plugins/x_redirect.php' and a class of 'Custom\Models\xMeta\plugins\x_redirect'
     */
    function PageMetaTags()
    {
        //Get a pointer to the Code Igniter singleton. 
        $CI =& get_instance();

        //Use the framework method '_getMetaInformation' to retrieve an array of
        //attributes and values for the current page. The framework parses and retrieves
        //this information automatically
        $meta = $CI->_getMetaInformation();

        //Iterate through the attributes.
        foreach($meta as $attribute => $value)
        {
            //Only look for custom handlers
            if(!\RightNow\Utils\Text::beginsWith($attribute, 'x_'))
            {
                continue;
            }
            
            //Load Plugin class and execute
            $class = sprintf('\Custom\Models\xMeta\plugins\%s', $attribute);
            if(!class_exists($class))
            {
                //Attempt to load plugin class
                $path = sprintf(APPPATH . 'models/custom/xMeta/plugins/%s.php', $attribute);
                if(file_exists($path))
                {
                    require_once $path;
                }

                if(!class_exists($class))
                {
                    $warning = 'xMeta Plugin Not Loaded. Plugin: ' . $attribute;
                    \RightNow\Utils\Framework::addErrorToPageAndHeader($warning);
                    \RightNow\Utils\Framework::logMessage($warning);
                    \RightNow\Utils\Framework::logMessage('  Expected Class: ' . $class);
                    \RightNow\Utils\Framework::logMessage('  Expected Path: ' . $path);
                    
                    continue;
                }
            }
            
            if(!in_array('Custom\Models\xMeta\classes\PluginInterface', class_implements($class)))
            {
                $warning = 'xMeta Plugin does not implement PluginInterface. Class: ' . $attribute;
                \RightNow\Utils\Framework::addErrorToPageAndHeader($warning);
                \RightNow\Utils\Framework::logMessage($warning);
                \RightNow\Utils\Framework::logMessage('  Plugin Class: ' . $class);
                continue;
            }
            
            
            try
            {
                $instance = new $class();
                $instance->executeHandler($value);
                
            } catch (\Exception $ex) {
                $error = sprintf("xMeta Error (%s): %s", $attribute, $ex->getMessage());
                \RightNow\Utils\Framework::addErrorToPageAndHeader($error);
                \RightNow\Utils\Framework::logMessage($error);
            }
        }
    }
    
}
