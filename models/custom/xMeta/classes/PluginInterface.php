<?php
/**
 * xMeta Component for Oracle Service Cloud CP3
 * Provides the ability to define custom Page Meta tags.
 * Custom tags handlers are defined through a plugin interface.
 * Based on http://cxdeveloper.com/article/creating-custom-page-meta-tag-attributes-cp3
 * 
 * @copyright (c) 2014, 45 North Solutions LLC
 * @package https://bitbucket.org/45north/xmeta
 * @version 2.0.0
 * @license MIT
 * @author Andy Rowse <andy45n.co>
 */

namespace Custom\Models\xMeta\classes;

/**
 * Interface that must be implemented by all xMeta plugins
 */
interface PluginInterface
{
    public function executeHandler($value = null);
}