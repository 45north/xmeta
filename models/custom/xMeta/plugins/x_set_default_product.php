<?php
/**
 * xMeta Component for Oracle Service Cloud CP3
 * Provides the ability to define custom Page Meta tags.
 * Custom tags handlers are defined through a plugin interface.
 * Based on http://cxdeveloper.com/article/creating-custom-page-meta-tag-attributes-cp3
 * 
 * @copyright (c) 2014, 45 North Solutions LLC
 * @package https://bitbucket.org/45north/xmeta
 * @version 2.0.0
 * @license MIT
 * @author Andy Rowse <andy45n.co>
 */

namespace Custom\Models\xMeta\plugins;

use RightNow\Utils\Url as Url;

require_once APPPATH . 'models/custom/xMeta/classes/PluginInterface.php';

/**
 * xMeta Attribute Handler
 * Forces a product (p parameter) to be set on a page if one isn't explicitly 
 * passed through the URL. 
 */
class x_set_default_product implements \Custom\Models\xMeta\classes\PluginInterface
{   
    public function executeHandler($value = null)
    {
        require_once APPPATH . 'libraries/Params.php';
        if(!Url::getParameter('p') && $value)
        {
            \Custom\Libraries\Params::ForceOrOverrideUriParameter("p", $value);
        }   
    }
}

