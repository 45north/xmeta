<?php
/**
 * xMeta Component for Oracle Service Cloud CP3
 * Provides the ability to define custom Page Meta tags.
 * Custom tags handlers are defined through a plugin interface.
 * Based on http://cxdeveloper.com/article/creating-custom-page-meta-tag-attributes-cp3
 * 
 * @copyright (c) 2014, 45 North Solutions LLC
 * @package https://bitbucket.org/45north/xmeta
 * @version 2.0.0
 * @license MIT
 * @author Andy Rowse <andy45n.co>
 */

namespace Custom\Models\xMeta\plugins;

use RightNow\Utils\Url as Url;

require_once APPPATH . 'models/custom/xMeta/classes/PluginInterface.php';

/**
 * xMeta Attribute Handler
 * Redirect page to the specified path. Supports dynamically resolved placeholders
 * sourced from the URL, GET Params, or a configuration value
 * Performs a 302 redirect to the path specified by the attribute value
 */
class x_redirect implements \Custom\Models\xMeta\classes\PluginInterface
{   
    public function executeHandler($redirect_url_templated = null)
    {
        if(!$redirect_url_templated)
        {
            \RightNow\Utils\Framework::addDevelopmentHeaderWarning('No valued passed for x_redirect page meta tag');
            return;
        }
        
        $regex = "/{{(get:|config:|url:|all:)(\w*?)}}/";
        $method = 302;
        
        $redirect_url_resolved = preg_replace_callback(
            $regex,
            function($match)
            {
                switch(strtolower($match[1]))
                {
                    case 'all:':
                        switch(strtolower($match[2]))
                        {
                            case 'get':
                                return http_build_query($_GET);
                                break;
                            case 'url':
                                return Url::getParameterString();
                                break;
                        }
                        break;
                    case 'get:':
                        return get_instance()->input->get($match[2]);
                        break;
                    case 'config:':
                        //See if config exists
                        try
                        {
                            /* @var $redirect_url_resolved Configuration */
                            $dynConfig = \RightNow\Connect\v1_3\Configuration::fetch($match[2]);
                            return $dynConfig->Value;
                        } 
                        catch (\Exception $ex) 
                        {
                            Framework::logMessage($ex);
                            return "";
                        }
                        break;
                    case 'url:':
                        return Url::getParameter($match[2]);
                        break;
                    default:
                        return "";
                        break;
                }
            }, 
            $redirect_url_templated
        );
            
        header('Location: ' . $redirect_url_resolved, true, (int) $method);
        exit;
    }
}

