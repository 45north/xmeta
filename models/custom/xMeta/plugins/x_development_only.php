<?php
/**
 * xMeta Component for Oracle Service Cloud CP3
 * Provides the ability to define custom Page Meta tags.
 * Custom tags handlers are defined through a plugin interface.
 * Based on http://cxdeveloper.com/article/creating-custom-page-meta-tag-attributes-cp3
 * 
 * @copyright (c) 2014, 45 North Solutions LLC
 * @package https://bitbucket.org/45north/xmeta
 * @version 2.0.0
 * @license MIT
 * @author Andy Rowse <andy45n.co>
 */

namespace Custom\Models\xMeta\plugins;

use RightNow\Utils\Url as Url;

require_once APPPATH . 'models/custom/xMeta/classes/PluginInterface.php';

/**
 * xMeta Attribute Handler
 * Development Only check. When applied to a page,
 * it can only be viewed in development mode.
 * This is mainly done for things like Account management
 * to allow for logging in directly without SAML.
 * Takes a value parameter that represents the redirect URL. If one is not
 * specified, the user is redirected to the 404 page.
 */
class x_development_only implements \Custom\Models\xMeta\classes\PluginInterface
{   
    /**
     * 
     * @param string $value URL to where the user should be redirected
     * @return void
     */
    public function executeHandler($value = null)
    {
        if(IS_DEVELOPMENT)
        {
            return;
        }
        
        $redirect_url = $value ?: "/app/error404";
        $method = 302;
            
        header('Location: ' . $redirect_url, true, (int) $method);
        exit;
    }
}

