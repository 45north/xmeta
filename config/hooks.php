<?php

/*
 * EXAMPLE
 * Activate xMeta tag component
 */

$rnHooks['pre_page_render'][] = array(
    'class' => 'PrePageRender',
    'function' => 'PageMetaTags',
    'filepath' => 'xMeta/hooks'
);
