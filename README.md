# README #

This MIT-licensed component for CP3 is an extensible implementation of Custom Page Meta Tags as described by this CXDeveloper.com post (http://cxdeveloper.com/article/creating-custom-page-meta-tag-attributes-cp3).

Custom meta tag handlers are written as plugins and stored in the models/custom/xMeta/plugins folder. 

All plugins must:

* Implement the Custom\Models\xMeta\classes\PluginInterface interface, which is found in the models/custom/xMeta/classes/PluginInterface.php file.
* Be located in the /models/custom/xMeta/plugins directory
* Have a class name that matches the attribute name and be inside of the Custom\Models\xMeta\plugins namespace. Example x_redirect == \Custom\Models\xMeta\plugins\x_redirect
* Have a file name that matches the attribute name. Example x_redirect == models/custom/xMeta/plugins/x_redirect.php

## Installation ##

1. Add the models/custom/xMeta/* files to your project
1. Add the pre_page_render hook registration to your hooks.php. See the projects config/hooks.php file for this code.
1. Write and add new plugins to the models/custom/xMeta/plugins directory a needed, or utilize the examples provided in the plugins directory.

## Example Plugins ##

### x_redirect ###
Redirect page to the path specified by the attribute's value. Supports dynamically resolved placeholders variables sourced from the URL, GET Params, or a configuration value.

### x_development_only ###
Development Mode Only check. When applied to a page, it can only be viewed in development mode. This is mainly done for things like the Account management pages to allow for logging in directly without SAML or PTA. Takes a value parameter that represents the redirect URL. If one is not specified, the user is redirected to the 404 page.